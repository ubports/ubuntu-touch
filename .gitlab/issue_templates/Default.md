<!------------------------------------------------------------------------------
--------------------------------------------------------------------------------

IMPORTANT: Please file PINEPHONE ISSUES here: https://gitlab.com/ubports/community-ports/pinephone/-/issues

Thank you for wanting to file an issue! If you have a minute, please take the time to read our guide on writing a good bug-report:

    https://docs.ubports.com/en/latest/contribute/bugreporting.html

If not and you just want to get this over with, that's also quite alright. But please try to format your issue correctly, to make it easier for us to read. Below, we provide a template for a feature-request and a bug-report. Use common sense to determine what category your issue falls under and delete the template you don't need. Also, you don't need to stick to the templates under all circumstances. If you don't need a heading, remove it. If you think a new section will provide valuable information, add a new heading. We're not dogmatists, we're pragmatists who just want to be able to efficiently work with new issues.

--------------------------------------------------------------------------------
------------------------------------------------------------------------------->

<!--
 --- If you're filing a feature request, consider selecting "Feature request" as
 --- the issue template above.
 -->

<!------------------------------------------------------------------------------
-------- BUG-REPORT ------------------------------------------------------------
--------------------------------------------------------------------------------
-------- Something doesn't work the way you want it? That's a bug. -------------
--------------------------------------------------------------------------------
------------------------------------------------------------------------------->

- Device:
- Channel:
- Build:

### Steps to reproduce
<!-- Describe what causes your bug to occur -->


### Expected behavior
<!-- Describe what you'd expect to happen -->


### Actual behavior
<!-- Describe what actually happens instead -->


### Logfiles and additional information
<!-- https://docs.ubports.com/en/latest/contribute/bugreporting.html#getting-logs -->