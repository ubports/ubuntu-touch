# Ubuntu Touch

## A simple and beautiful mobile OS for everyone!
[Ubuntu Touch](https://ubports.com) is the touch-friendly mobile version of the [Ubuntu](https://ubuntu.com) operating system. Originally designed and developed by Canonical, it now lives on in the UBports community.

## How to use this repo

This repository is used as the "inbox" repository; the initial place for user-reported bugs and feature requests in Ubuntu Touch OS. It also acts as the catch-all place for Ubuntu Touch's issues that does not fit any specific repository.

Please read our [guide on writing a good bug report](https://docs.ubports.com/en/latest/contribute/bugreporting.html) before creating a new issue. To better understand the lifecycle of your issue, refer to our [issue tracking guidelines](https://docs.ubports.com/en/latest/about/process/issue-tracking.html). Thank you!

### Shortcut to issue milestones

[Milestones](https://gitlab.com/groups/ubports/-/milestones). [What's the policy](https://docs.ubports.com/en/latest/about/process/issue-tracking.html#milestones)?

### Labels and Filtering

You can use GitLab's filtering syntax to reduce clutter. [Here's a brief explaination of the standard labels and what they mean](https://docs.ubports.com/en/latest/about/process/issue-tracking.html#labels).

Additionally, the following special labels are used:

#### Critical


 - [critical (devel)](https://gitlab.com/groups/ubports/-/issues/?sort=created_date&state=opened&label_name[]=critical%20(rc)): This critical issue that only occurs on the devel channel is blocking the release of the next rc image.
 - [critical (rc)](https://gitlab.com/groups/ubports/-/issues/?sort=created_date&state=opened&label_name[]=critical%20(rc)): This critical issue that only occurs on the devel and rc channel is blocking the release of the next stable release. Usually, issues that can not simply be moved to a different release and have the power to postpone the release are labeled this.

#### Team

 - [team: ui](https://gitlab.com/groups/ubports/-/issues/?sort=created_date&state=opened&label_name[]=team%3A%20ui): This issue falls under the responsibility of the user-interface team.
 - [team: middleware](https://gitlab.com/groups/ubports/-/issues/?sort=created_date&state=opened&label_name[]=team%3A%20middleware): This issue falls under the responsibility of the middleware team.
 - [team: hal](https://gitlab.com/groups/ubports/-/issues/?sort=created_date&state=opened&label_name[]=team%3A%20hal): This issue falls under the responsibility of the hardware abstraction layer team.
---

*Note:* This repository does not contain the actual Ubuntu Touch code. Duh.
